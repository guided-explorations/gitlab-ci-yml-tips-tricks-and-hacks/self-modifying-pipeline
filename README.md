## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/gitlab-ci-yml-tips-tricks-and-hacks/self-modifying-pipeline)

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested.

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

* **Product Manager For This Guided Exploration**: Xiaogang Wen (@xiaogang_gitlab)

* **Publish Date**: 2020-04-02

* **GitLab Version Released On**: 12.10

* **GitLab Edition Required**: 

  * For overall solution: [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/) 

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

* **References and Featured In**:

  
## Demonstrates These Design Requirements, Desirements and AntiPatterns

- **Development Pattern:** the need to generate pipeline code while in the self same pipeline.
- **GitLab Development Pattern:** pipeline code generated within the same pipeline it needs to be used in discussed here: https://docs.gitlab.com/ee/ci/yaml/#trigger-child-pipeline-with-generated-configuration-file.
- **Anti-Pattern:** self-modifying code does not strictly align with declarative coding principles nor GitOps - this example shows what is possible when no other approach will meet the requirements - but direct declarative code is explicit and easier to understand if it can meet requirements.

#### GitLab CI Functionality:

- **.gitlab-ci.yml** "includes" ![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)can be a file in the same repo, another repo or an https url
  - Used to enable manage main build templates (gitlab ci custom functions) to have more change controls over them.
- **.gitlab-ci.yml** "trigger:include:" ![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png) enabling dynamically generated yml to be run.

## Using This Pattern:
- To see more than one sub-job, specify `JOB_NUM` for the number of jobs in the downstream pipeline before you run the pipeline
